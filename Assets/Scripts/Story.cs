﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Story : MonoBehaviour {

	public Text storyText;

	/*// Star Wars Stuff
	private float duration;
	private RectTransform rectTransform;
	private Vector2 textStartPosition, textEndPosition;
	private Color textStartColor, textEndColor;
	private Coroutine TextCoroutine;
*/
	// Story text narration for Intro and Munich
	string storyMain1 = "It’s the year 2020. Switzerland’s Large Hadron Collider creates something\n"+
						"similar to a black hole, while looking to discover dark matter. A number of\n"+
						"humanoids emerge from the hole and announce the existence of a parallel\n"+
						"world inhabited by them, while declaring war to our species.";
	string storyMain2 = "Humanity resists the first waves of enemies called “Elementalites”, whose\n"+
						"powers allow them to control elements like air, ground, water and fire,\n"+
						"start to defeat our conventional weapons. The world starts to use nuclear\n"+
						"weapons in a desperate attempt to stop the invaders, with little success.";
	string storyMain3 = "Prominent scientist Solz Tracer is put in charge of an operation to put\n"+
						"Elementalites’ fallen weapons to use by humans. These, however, consume\n"+
						"and kill their earthling possessors, reason why the program is shut down\n"+
						"and its crew, including Solz and his workers, disappear. Mankind is on the\n"+
						"verge of extinction.";
	string storyMunich1 = "After a couple of years that presented desperate defenses carried out by\n"+
							"humanity, Dr. Solz reappears in Munich’s Nymphenburg Castle, Germany, one\n"+
							"of Europe’s last defense bastions. The scientist is followed by a group of\n"+
							"young men and women, who seem to wield non-conventional weapons and are\n"+
							"able to control elements, just like the Elementalites do.";
	string storyMunich2 = "Rumor says that they are humanity’s last hope and that they’ll attempt to\n"+
							"reach the Large Hadron Collider in Geneva, Switzerland, to close the portal\n"+
							"to the other universe and save mankind.\n"+
							"Will they succeed in this near-impossible task, or will they fail to save\n"+
							"the world as we know it?";

	//Story text narration for Zurich
	string storyZurich1 = "After endless skirmishes between Dr Solz’s group and the city’s invaders and\n"+
							"achieving to build a temporary safe zone, our protagonists finally reach\n"+
							"Munich’s own airport, controlled by European Union forces. The EU decides to\n"+
							"grant a military aircraft transport to Solz and his crew, which would fly\n"+
							"them to the humanity’s last defended settlement in Switzerland: Zurich.";
	string storyZurich2 = "Before landing, the group decides to reach a secret spot in the city, where a\n"+
							"Swiss rescue chopper lies, and which could take them even closer to the\n"+
							"Large Hadron Collider. At landing, however, they realize that the city hosts\n"+
							"much more enemies than expected, which will make their mission even more\n"+
							"difficult…";

	// Story text narration for Berna
	string storyBerna1 = "With a brave effort and fierce combats throughout Zurich, Solz and his crew\n"+
							"reach the helicopter, hoping to reach Geneva after a few hours of flying.\n"+
							"As they were flying above Switzerland’s capital, Bern, something resembling\n"+
							"a massive rock suddenly strikes the aircraft, destroying the main rotor and\n"+
							"forcing the chopper to fall without any possibility of controlling it.";
	string storyBerna2 = "It crashes after a while of spinning out of control, in what it looks to be\n"+
							"Bern’s own Gantrisch nature park.\n"+
							"Some members of the crew are injured, and some are dead. They are\n"+
							"surrounded by a forest, without a clear way out and probably nearby a\n"+
							"powerful enemy. Will this be their impending doom?";

	// Story text narration for continue
	string continue1 = "Our protagonists manage to defeat the giant Elementalite, but with great\n"+
						"sacrifices made. They are exhausted, without resources, some dead, others\n"+
						"injured and Geneva is still many miles away from their position, full of more\n"+
						"numerous and powerful enemies, which are guarding the entrance to their\n"+
						"world.";
	string continue2 = "It will be your fate to reach the Large Hadron Collider by any possible means.\n"+
						"Will you survive, defeat your enemies and become a hero, or will you crush\n"+
						"humanity’s last hope in repelling their invaders?";

	// Story text general
	float startDelay = 2f;
	float typeDelay = 0.05f;
	string[] storyParts = new string[2];
	Color keepTextColor; // To set color to text after clearing it

	// Story images for narration Intro and Munich
	public Image storyImg;
	public Sprite image1;
	public Sprite image2;
	public Sprite image3;
	public Sprite image4_M;
	public Sprite image5_M;

	// Story images for narration Zurich
	public Sprite image6_Z;
	public Sprite image7_Z;

	// Story images for narration Berna
	public Sprite image8_B;
	public Sprite image9_B;

	// Story images for narration continue
	public Sprite image10_C;
	public Sprite image11_C;

	// Story images general
	List<Sprite> sprites = new List<Sprite>();
	float fadeSpeed = 0.003f;
	bool fade;

	// Fade in and fade out panel
	public Image shadeImg;
	bool shade;
	float shadeSpeed = 0.0005f;

	//audio
	public AudioSource typing;
	public AudioSource music = new AudioSource();
	public AudioSource main;
	public AudioSource zurich;
	public AudioSource bern;
	public AudioSource continmusic;
	public float musicFadeSpeed = 1f;

	
	void Start() 
	{
		/*// Star Wars Stuff
		rectTransform = storyText.GetComponent<RectTransform>();
		textStartPosition = rectTransform.anchoredPosition;
		textEndPosition = new Vector2(textStartPosition.x,Screen.height/2);
		textStartColor = storyText.color;
		textEndColor = new Color(textStartColor.r,textStartColor.g,textStartColor.b,0f);
		duration = 15f;
		TextCoroutine = StartCoroutine(ShowText());
*/

		switch(StaticVariables.storyScene)
		{
		case 1:
			music = main;
			music.volume = 0.8f;
			music.Play ();
			storyParts = new string[5];
			storyParts[0] = storyMain1;
			storyParts[1] = storyMain2;
			storyParts[2] = storyMain3;
			storyParts[3] = storyMunich1;
			storyParts[4] = storyMunich2;
			sprites.Add(image1);
			sprites.Add(image2);
			sprites.Add(image3);
			sprites.Add(image4_M);
			sprites.Add(image5_M);
			break;
		case 2:
			music = zurich;
			music.volume = 0.8f;
			music.Play ();
			storyParts[0] = storyZurich1;
			storyParts[1] = storyZurich2;
			sprites.Add(image6_Z);
			sprites.Add(image7_Z);
			break;
		case 3:
			music = bern;
			music.volume = 0.8f;
			music.Play ();
			storyParts[0] = storyBerna1;
			storyParts[1] = storyBerna2;
			sprites.Add(image8_B);
			sprites.Add(image9_B);
			break;
		case 4:
			music = continmusic;
			music.volume = 0.8f;
			music.Play ();
			storyParts[0] = continue1;
			storyParts[1] = continue2;
			sprites.Add(image10_C);
			sprites.Add(image11_C);
			break;
		default:
			Application.LoadLevel("MainMenu");
			break;
		}

		storyImg.color = Color.clear;
		keepTextColor = storyText.color;
		StartCoroutine("TypeIn");
		shade = true;
	}

	/*// Star Wars Stuff
	IEnumerator ShowText() 
	{
		storyText.enabled = true;
		
		float elapsedTime = 0;
		
		while (elapsedTime < duration) 
		{
			float t = elapsedTime / duration; //0 means the animation just started, 1 means it finished
			rectTransform.anchoredPosition = Vector2.Lerp(textStartPosition,textEndPosition,t);
			//storyText.color = Color.Lerp(textStartColor,textEndColor,t); 
			elapsedTime += Time.deltaTime;
			yield return null;
		}
		
		storyText.enabled = false;
	}
*/

	void Update ()
	{
		FadeInFadeOut();
		ColorChange();
	}

	IEnumerator FadeMusic()
	{
		while(music.volume < 0.35F)
		{
			music.volume = Mathf.Lerp(music.volume, 0.35f, musicFadeSpeed * Time.deltaTime);
			yield return 0.35f;
		}
		music.volume = 0.35f;
		//perfect opportunity to insert an on complete hook here before the coroutine exits.
	}
	
	IEnumerator TypeIn() 
	{
		yield return new WaitForSeconds(startDelay);
		shadeImg.gameObject.SetActive(false);

		for(int i = 0; i < storyParts.Length; i++)
		{
			storyImg.overrideSprite = sprites[i];
			fade = true;
			storyText.color = keepTextColor;
			for(int j = 0; j < storyParts[i].Length + 1; j++)
			{
				typing.Play ();
				storyText.text = storyParts[i].Substring(0, j);
				yield return new WaitForSeconds(typeDelay);
			}
			typing.Stop ();
			yield return new WaitForSeconds(3f);

			if(i < storyParts.Length - 1)
			{

				fade = false;
				yield return new WaitForSeconds(startDelay);
				storyText.text = "";
			}
		}

		StartCoroutine (FadeMusic());
		shadeImg.gameObject.SetActive(true);
		shade = false;
		yield return new WaitForSeconds(startDelay);
		Skip();
	}

	void ColorChange() 
	{
		if(fade)
		{
			storyImg.color = Color.Lerp(storyImg.color, Color.white, fadeSpeed + Time.deltaTime);
		}
		
		if(!fade)
		{
			storyImg.color = Color.Lerp(storyImg.color, Color.clear, fadeSpeed + Time.deltaTime);
			storyText.color = Color.Lerp(storyText.color, Color.clear, fadeSpeed + Time.deltaTime);
		}
	}


	
	void FadeInFadeOut()
	{
		if(shade)
		{
			shadeImg.color = Color.Lerp(shadeImg.color, Color.clear, shadeSpeed + Time.deltaTime);
		}

		if(!shade)
		{
			shadeImg.color = Color.Lerp(shadeImg.color, Color.black, fadeSpeed + Time.deltaTime);
		}
	}

	public void Skip()
	{
		Application.LoadLevel("LoadScreen");
		music.Stop ();
		typing.Stop ();
	}

}
