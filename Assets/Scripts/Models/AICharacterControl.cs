using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target; // target to aim for
		public float startChaceDistance = 20f;
		private EnemyHealth enemyHealth;
		private Animator animator;
		private float animCntr = 0.0f;
		private bool isBoss = false;
		float waitHit = 15f;
		public bool startTaunt = false;
		public AudioSource TauntBoss;
        // Use this for initialization
        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;

			animator = GetComponent<Animator>();
			enemyHealth = GetComponent<EnemyHealth> ();
			if(GetComponent<BossHealth>() != null){
				isBoss = true;
			}
        }

        // Update is called once per frame
        private void Update()
        {
			if(animCntr > 0.0f){
				animCntr -= Time.deltaTime;
			}else if(animCntr < 0.0f){
				animator.SetBool("Taunting", false);
			}

			//para el boss, para q no se mueva cuando ataca
			if(animator.GetBool("BossPunch") || animator.GetBool("Taunting")){
				agent.speed = 0;
			}else{
				agent.speed = 1;
			}

			float distance = Vector3.Distance(gameObject.transform.position, target.transform.position);
			if(distance <= startChaceDistance){
				agent.speed = 1;

				Quaternion orgRot = gameObject.transform.rotation; //rotacion inicial
				gameObject.transform.LookAt(target.transform.position); //Hacemos que Z apunte al Target

				Quaternion newRot = gameObject.transform.rotation; //rotacion que tendria si Z esta apuntando al target
				orgRot.y = newRot.y; //modificamos la rotacion en el eje Y de la rotacion inicial para que este viendo en la direccion del jugador
				gameObject.transform.rotation = orgRot; //seteamos la nueva rotacion 

				if (target != null)
	            {
					if(animCntr == 0.0f){
						animator.SetBool("Taunting", true);
						/*if(isBoss == true){
						TauntBoss.Play();
						}*/
						animCntr = animator.GetCurrentAnimatorClipInfo(0)[0].clip.length;
					}
					if(agent.isActiveAndEnabled)
					{
						agent.SetDestination(target.position);
						// use the values to move the character<= distance && playerHealth.itsAlive == true
						character.Move(agent.desiredVelocity, false, false);
					}

	            }
				else
	            {	
	                // We still need to call the character's move function, but we send zeroed input as the move param.
	                character.Move(Vector3.zero, false, false);
	            }
			}else{
					//ya no tiene que ir hacia adelante
					animator.SetFloat("Forward", 0.0f);
					agent.speed = 0;
			}

			//animacion de ataque si se encuentra dentro del rango de ataque (stopping distance del navmesh agent)
			if(isBoss && distance <= agent.stoppingDistance + 0.2f){
					animator.SetBool("BossPunch", true);
			}else{
					animator.SetBool("BossPunch", false);
			}
        }

        public void SetTarget(Transform target)
        {
            this.target = target;
        }
    }
}
