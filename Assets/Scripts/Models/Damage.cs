﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class Damage : MonoBehaviour {

	public HealthBar healthBar;
	public bool agua;
	public bool tierra;
	public bool fuego;
	public bool aire;

	private string elemento;

	// Use this for initialization
	void Start () {
		if(agua){
			elemento = ThirdPersonUserControl.Habilidades.habilidadAgua;
		}else if(tierra){
			elemento = ThirdPersonUserControl.Habilidades.habilidadTierra;
		}else if(fuego){
			elemento = ThirdPersonUserControl.Habilidades.habilidadFuego;
		}else if(aire){
			elemento = ThirdPersonUserControl.Habilidades.habilidadAire;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter (Collision obj){

		healthBar = GameObject.Find("PlayerModel").GetComponent<HealthBar> ();
		if (obj.gameObject.name == "PlayerModel") {
			healthBar.DamageAnim();
			healthBar.DamageLife(elemento);
			Destroy(this.gameObject);
		}
	}

}
