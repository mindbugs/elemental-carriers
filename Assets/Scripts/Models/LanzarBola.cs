﻿using UnityEngine;
using System.Collections;

public class LanzarBola : MonoBehaviour {
	public Rigidbody bola;
	float contador = 0f;
	float distance = 20;
	public Transform target;
	private EnemyHealth enemyHealth;
	public GameObject golem;
	public AudioSource waterattack;

	private bool charJustSeen; //la razon de esto es para saltarnos un ciclo desde que ve al usuario

	// Use this for initialization
	void Start () {
		enemyHealth = golem.GetComponent<EnemyHealth> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Vector3.Distance (gameObject.transform.position, target.transform.position) <= distance && enemyHealth.itsAlive == true){

			Vector3 playerPosition = target.transform.position;
			playerPosition.y += 1f;

			gameObject.transform.LookAt(playerPosition);

			if (contador <= 0f && charJustSeen && !golem.GetComponent<Animator>().GetBool("Taunting")) {
				StartCoroutine("trigerball");
				waterattack.pitch = Random.Range(0.75f, 1.3f);
				waterattack.Play ();
				enemyHealth.EnemyAtack ();
				contador = 5f;
			}else{
				charJustSeen = true;
			}

		}else{
			charJustSeen = false;
		} 

		if(contador > 0f) {
			contador -= Time.deltaTime;
			if(contador < 4.5f){
				enemyHealth.StopEnemyAtack ();
			}
		}
		
	}

	IEnumerator trigerball (){
		yield return new WaitForSeconds (0.6f); 
		lanzarBola ();
	}

	void lanzarBola(){

		Rigidbody pop = (Rigidbody) Instantiate (bola, gameObject.transform.position, Quaternion.identity);
		pop.velocity = gameObject.transform.forward * 10f;
		pop.useGravity = false;
	}

	
}
