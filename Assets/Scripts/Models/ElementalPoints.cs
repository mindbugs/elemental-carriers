using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ElementalPoints : MonoBehaviour {
	public Text textAire;
	public Text textTierra;
	public Text textAgua;
	public Text textFuego;

    float aire = 0;
	float tierra = 0;
	float agua = 0;
	float fuego = 0;


	public float chargeSpeed = 17f; //velocidad de puntos obtenidos por segundo
	public float maxCharge = 100f;

	private float MAX_POINTS = 99999f;
	
	
	private float[] cntersPoints = new float[4]; //simboliza la cantidad de cada texto
	
	private Text[] countersTexts = new Text[4];
	void Start () {
		countersTexts[0] = textAire;
		countersTexts[1] = textTierra;
		countersTexts[2] = textAgua;
		countersTexts[3] = textFuego;

		//esto se llama cada vez que el usuario empieza un nuevo escenario o se muere
		aire = StaticVariables.ptsElAire < 100f ? 100f : StaticVariables.ptsElAire;
		agua = StaticVariables.ptsElAgua < 100f ? 100f : StaticVariables.ptsElAgua;
		fuego = StaticVariables.ptsElFuego < 100f ? 100f : StaticVariables.ptsElFuego;
		tierra = StaticVariables.ptsElTierra < 100f ? 100f : StaticVariables.ptsElTierra;
	}
	
	
	void Update () {
		cargaPntosElementales();

		countersTexts [0].text = "" + (int) aire;
		countersTexts [1].text = "" + (int) tierra;
		countersTexts [2].text = "" + (int) agua;
		countersTexts [3].text = "" + (int) fuego;

		//actualizamos las variables estaticadas una vez cada frame 
		//esto podria mejorarse mucho en performance si se hacen estos seteos solo cuando el jugador
		//termina el escenario y no una vez por frame
		StaticVariables.ptsElAire = aire;
		StaticVariables.ptsElAgua = agua;
		StaticVariables.ptsElFuego = fuego;
		StaticVariables.ptsElTierra = tierra;
	}

	private void cargaPntosElementales(){
		if(aire < MAX_POINTS && aire < maxCharge) aire += chargeSpeed * Time.deltaTime;
		if(tierra < MAX_POINTS && tierra < maxCharge) tierra += chargeSpeed * Time.deltaTime;
		if(agua < MAX_POINTS && agua < maxCharge) agua += chargeSpeed * Time.deltaTime;
		if(fuego < MAX_POINTS && fuego < maxCharge) fuego += chargeSpeed * Time.deltaTime;
	}

	public void PerdidaDePuntosElementales(){
		float porcentajeAire;
		float porcentajeTierra;
		float porcentajeAgua;
		float porcentajeFuego;

		porcentajeAire = aire * 30f / 100f;
		aire = aire - porcentajeAire;

		porcentajeTierra = tierra * 30f / 100f;
		tierra = tierra - porcentajeTierra;

		porcentajeAgua = agua * 30f / 100f;
		agua = agua - porcentajeAgua;

		porcentajeFuego = fuego * 30f / 100f;
		fuego = fuego - porcentajeFuego;
	}


	public void elementalesDeAire(){

		aire = aire + 30f;
	}


	public void elementalesDeTierra(){
		
		tierra = tierra + 30f;
	}

	public void elementalesDeAgua(){
		
		agua = agua + 30f;
	}

	public void elementalesDeFuego(){
		
		fuego = fuego + 30f;
	}

	//gets..
	public float getAire(){
		return aire;
	}

	public float getTierra(){
		return tierra;
	}

	public float getAgua(){
		return agua;
	}

	public float getFuego(){
		return fuego;
	}

	//sets..
	public void setAire(float nuevoValor){
		aire = nuevoValor;
	}
	
	public void setTierra(float nuevoValor){
		tierra = nuevoValor;
	}
	
	public void setAgua(float nuevoValor){
		agua = nuevoValor;
	}
	
	public void setFuego(float nuevoValor){
		fuego = nuevoValor;
	}
}
