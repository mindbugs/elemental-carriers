﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class BossTaunt : MonoBehaviour {

	private Animator anim;
	float waitHit = 10f;
	private AICharacterControl aICharacterControl;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		isTaunt ();
	
	}

	void isTaunt (){
		if (waitHit <= 0)
		{
			anim.SetBool("Taunting", true);
			print ("Voy a gritar!!!");
			waitHit = 10f;
		}
		else
		{
			waitHit -= Time.deltaTime;
			anim.SetBool("Taunting", false);
		}
	}
	
}
