﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.UI;

public class InitPlayer : MonoBehaviour {

	private Animator playerAnimator;
	private ThirdPersonUserControl thirdPersonUserControl;

	public GameObject maleContainer;
	public GameObject femaleContainer;

	public Avatar maleAvatar;
	public Avatar femaleAvatar;

	public ParticleSystem maleParticleSystem;
	public ParticleSystem femaleParticleSystem;

	//armas hombre
	public GameObject maleAxe;
	public GameObject maleSword;
	public GameObject maleMace;
	public GameObject maleHammer;

	//armas mujer
	public GameObject femaleAxe;
	public GameObject femaleSword;
	public GameObject femaleMace;
	public GameObject femaleHammer;

	private Image deathCover;
	private bool unShaded = false;

	void Update () {
		if(!unShaded){ //para evitar gasto de recursos y para que no se 'unshade' cuando el jugador se muere.
			Color col = deathCover.color;
			col.a = (col.a - 1f * Time.deltaTime) <= 0 ? 0 :  col.a - 0.7f * Time.deltaTime; //para q el valor nunca sea menor q 0
			deathCover.color = col;
			unShaded = (col.a == 0);
		}
	}

	void Start () {
		deathCover = GameObject.Find("DeathCover").GetComponent<Image>();
		Color col = deathCover.color;
		col.a = 1f;
		deathCover.color = col;

		playerAnimator = GetComponent<Animator>();
		thirdPersonUserControl = GetComponent<ThirdPersonUserControl>();

		//Activamos el modelo del genero correcto y su sistema de particulas respectivo y el arma correcta
		if(StaticVariables.playerGenre == "male"){
			maleContainer.SetActive(true);
			playerAnimator.avatar = maleAvatar;
			thirdPersonUserControl.abilityParticles = maleParticleSystem;

			switch(StaticVariables.selectedWeapon){
			case StaticVariables.Weapon.Axe:
				maleAxe.SetActive(true);
				break;
			case StaticVariables.Weapon.Hammer:
				maleHammer.SetActive(true);
				break;
			case StaticVariables.Weapon.Sword:
				maleSword.SetActive(true);
				break;
			case StaticVariables.Weapon.Mace:
				maleMace.SetActive(true);
				break;
			}

		}else if(StaticVariables.playerGenre == "female"){
			femaleContainer.SetActive(true);
			playerAnimator.avatar = femaleAvatar;	
			thirdPersonUserControl.abilityParticles = femaleParticleSystem	;	

			switch(StaticVariables.selectedWeapon){
			case StaticVariables.Weapon.Axe:
				femaleAxe.SetActive(true);
				break;
			case StaticVariables.Weapon.Hammer:
				femaleHammer.SetActive(true);
				break;
			case StaticVariables.Weapon.Sword:
				femaleSword.SetActive(true);
				break;
			case StaticVariables.Weapon.Mace:
				femaleMace.SetActive(true);
				break;
			}

		}


	}

}
