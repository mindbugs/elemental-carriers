﻿using UnityEngine;
using System.Collections;

public class BossAtack : MonoBehaviour {

	private Animator anim;
	public Transform target;
	HealthBar healthBar;
	BossHealth bossHealth;
	int doDamage; // Counter for boss damage, 1 and 2 = 20f, 3 = 40f
	public GameObject boss;
	private NavMeshAgent navAgent;
	private float animCntr = 0.0f;
	private float damCntr = 0.0f;

	//sonidos
	public AudioSource impactboss;

	void Start ()
	{
		navAgent = boss.GetComponent<NavMeshAgent>();
		anim = boss.GetComponent<Animator> ();
		healthBar = GameObject.Find("PlayerModel").GetComponent<HealthBar> ();
	}
	
	void Update ()
	{
		// Checks if boss should attack or follow player
		BossAtacks ();

		//si esta recibiendo danno
		if(damCntr > 0.0f){
			damCntr -= Time.deltaTime;
		}else if(damCntr < 0.0f){
			anim.SetBool("DamagePlayer", false);
			damCntr = 0.0f;
		}
	}
	/*IEnumerator Swing(){
		swingboss.Play ();
		yield return new WaitForSeconds (2.0f);
		swingboss.Play ();
		yield return new WaitForSeconds (1.2f);
		swingboss.Play ();
		yield return new WaitForSeconds (0.8f);
	}*/


	void BossAtacks ()
	{
		if(animCntr > 0.0f){
			animCntr -= Time.deltaTime;
		}else if(animCntr < 0.0f){
			anim.SetBool("BossPunch", false);
			navAgent.speed = 1;
			animCntr = 0.0f;
		}

		// Checks distancebetween boss and player to start attacking or stop
		if (Vector3.Distance(gameObject.transform.position, target.transform.position) <= 2){
			if(animCntr == 0.0f){
				anim.SetBool("BossPunch", true);
				//StartCoroutine(Swing ());
				animCntr = anim.GetCurrentAnimatorClipInfo(0)[0].clip.length;
				navAgent.speed = 0;
			}
		}

		// Reset doDamage counter if player is out of range
		if(Vector3.Distance(gameObject.transform.position, target.transform.position) > 3)
			doDamage = 1;
	}

	void OnTriggerEnter(Collider col)
	{
		// When player is hit apply damage
		if(col.gameObject.tag == "Player")
		{
			impactboss.pitch = Random.Range(0.75f, 1.3f);
			impactboss.Play();
			healthBar.DamageAnim();
			if(doDamage == 3) healthBar.playerEnergy -= 40f; // Third combo attack do more damage
			else healthBar.playerEnergy -= 20f;
			doDamage ++;
		}
	}

	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag == "Player")
		{
			// Reset doDamage counter and stop attacking
			if(doDamage == 3)
			{
				anim.SetBool("BossPunch", false);
				doDamage = 1;
			}
		}
	}

	public void DamageAnim (){
		if(damCntr <= 0.0f){
			anim.SetBool("DamagePlayer", true);
			damCntr = anim.GetCurrentAnimatorClipInfo(0)[0].clip.length/2;
		}
		
	}

}
