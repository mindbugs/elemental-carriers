﻿  using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class EnemyHealth : MonoBehaviour {

    private float max_Health = 400f;
    public float moreHealthRandom;
    public float cur_Health = 0f;
    public GameObject healthBar;
	public bool itsAlive = true;
	private bool EnemyItsAtack;
	private Animator anim;
    public Transform target;
    private ThirdPersonUserControl thirdPersonUserControl;
    private ElementalPoints elementalPoints;
	public float segundosMuerte = 3f;
	public AudioSource attackhit;
	private float animCntr = 0f;


	// This variable will set golem type for bonus elemental points
	// 1: Water, 2: Air, 3: Fire, 4: Earth
	public int golemType;

    void Start ()
	{
        moreHealthRandom = Random.Range(1F, 300F);
        cur_Health = max_Health + moreHealthRandom;
        InvokeRepeating("decreasehealth", 1f, 1f);
	    thirdPersonUserControl =  GameObject.Find ("PlayerModel").GetComponent<ThirdPersonUserControl> ();
		anim = GetComponent<Animator> ();
		elementalPoints = GameObject.Find ("PlayerModel").GetComponent<ElementalPoints> ();
    }

    void Update ()
	{

		if(animCntr > 0.0f){
			animCntr -= Time.deltaTime;
		}else if(animCntr < 0.0f){
			anim.SetBool("DamagePlayer", false);
		}


	    enemyIsAtack ();

		if (cur_Health <= 0)
		{
			// When life reaches 0, enemy do dead animation and start counting dead time.
			anim.SetBool ("IsDead", true);
			segundosMuerte -= Time.deltaTime;
		}
		if (segundosMuerte <= 0)
		{
			// When dead time runs out player will gain extra elemental points
			// increase enemy defeated counter and destroy the enemy.
			BonusElementalPoints();
			StaticVariables.enemyCount ++;
			Destroy(gameObject);
		}
	}

	// This method grants bonus points for killing a specific type of golem
	void BonusElementalPoints()
	{
		switch(golemType)
		{
		case 1:
			elementalPoints.elementalesDeAgua();
			break;
		case 2:
			elementalPoints.elementalesDeAire();
			break;
		case 3:
			elementalPoints.elementalesDeFuego();
			break;
		case 4:
			elementalPoints.elementalesDeTierra();
			break;
		default:
			break;
		}
	}

	public void EnemyAtack (){
		anim.SetBool ("enemyPunch", true);
	}

	public void StopEnemyAtack (){
		anim.SetBool ("enemyPunch", false);
	}

    void decreasehealth() {
		if (itsAlive == true && cur_Health > 1f) {
			float calc_Health = cur_Health / (max_Health + moreHealthRandom);
			SetHealthBar (calc_Health);
		}else if(itsAlive == true){
			SetHealthBar (0);
			itsAlive = false;
		}
    }

	void enemyIsAtack (){
		if (Vector3.Distance(gameObject.transform.position, target.transform.position) <= 2 && thirdPersonUserControl.attacking == true){
			attackhit.pitch = Random.Range(0.75f, 1.3f);
			attackhit.Play ();
			DamageAnim();
			cur_Health -= thirdPersonUserControl.habFuegoActivada ? 200 * Time.deltaTime : 50 * Time.deltaTime;
		}
	}

    public void SetHealthBar(float myHealth) {
            healthBar.transform.localScale = new Vector3(myHealth, healthBar.transform.localScale.y, healthBar.transform.localScale.z);
    }

	public void DamageAnim (){
		if(animCntr <= 0.0f ){
			anim.SetBool("DamagePlayer", true);
			animCntr = anim.GetCurrentAnimatorClipInfo(0)[0].clip.length/2;
		}
		
	}

}
