using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class BossHealth : MonoBehaviour {
	
	private float max_Health = 4000f;
	public float cur_Health = 0f;
	public GameObject healthBar;
	public bool comienza;
	//float waitHit = 10f;
	public bool itsAlive = true;
	private Animator anim;
	public float damagePorcentage;
	public Transform target;
	private ThirdPersonUserControl thirdPersonUserControl;
	public float segundosMuerte = 5f;
	public AudioSource attackhit;
	private float animCntr = 0f;
	
	// Use this for initialization
	void Start () {
		cur_Health = max_Health;
		InvokeRepeating("decreasehealth", 1f, 1f);
		thirdPersonUserControl =  GameObject.Find ("PlayerModel").GetComponent<ThirdPersonUserControl> ();
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		bossIsAtack ();
		Dead ();
	}
	
	void bossIsAtack (){
		if (Vector3.Distance(gameObject.transform.position, target.transform.position) <= 3 && thirdPersonUserControl.attacking == true){
			attackhit.pitch = Random.Range(0.75f, 1.3f);
			attackhit.Play ();
			//DamageAnimationBoss();
			cur_Health -= thirdPersonUserControl.habFuegoActivada ? 100 * Time.deltaTime : 50 * Time.deltaTime;
		}
	}

	void Dead(){
		if (cur_Health <= 0) {
			anim.SetBool ("IsDead", true);
			segundosMuerte -= Time.deltaTime;
		}
		if (segundosMuerte <= 0) {
			Destroy(gameObject);
			StaticVariables.enemyCount ++;
		}
	}

	void decreasehealth() {
		if (itsAlive == true && cur_Health > 1f) {
			float calc_Health = cur_Health / (max_Health);
			SetHealthBar (calc_Health);
		}else if(itsAlive == true){
			SetHealthBar (0);
			itsAlive = false;
		}
	}

	public void DamageAnimationBoss (){
		/* Esto lo vamos a dejar?
		 * anim.SetBool("DamagePlayer", true); y luego anim.SetBool("DamagePlayer", false);
		 * nunca va a hacer q la animacion se reproduzca


		damagePorcentage =  Random.Range (1,70);
		if (damagePorcentage > 10){
			anim.SetBool("DamagePlayer", true); 
			animCntr = anim.GetCurrentAnimatorClipInfo(0)[0].clip.length/2;
			anim.SetBool("DamagePlayer", false);
		}

		*/
	}

	public void SetHealthBar(float myHealth) {
		healthBar.transform.localScale = new Vector3(myHealth, healthBar.transform.localScale.y, healthBar.transform.localScale.z);
	}
}