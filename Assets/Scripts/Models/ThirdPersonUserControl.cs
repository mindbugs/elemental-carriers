using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;
using UnityEngine.UI;
namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
		public float rotationSpeed = 4.5f;
        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.

		private Vector3 m_TargetAngles; 		// for managing mouse rotation speed
        private Vector3 m_FollowAngles;
		private Vector3 m_FollowVelocity;
		private Quaternion m_OriginalRotation;
		public int checkpoint;
		public ArrayList noCheck = new ArrayList();

		public ParticleSystem abilityParticles;
		private Animator anim;
		public bool attacking = false;
		private float dampingTAttack = 0.0f;
		//public bool isAtacking = false;
		public AudioSource attackswing;
		public AudioSource waterattack;
		public AudioSource firecast;
		public AudioSource firecharge;
		public AudioSource aircast;
		public AudioSource airready;
		public AudioSource watercast;
		public AudioSource waterready;
		public AudioSource earthcast;
		public AudioSource earthready;
		public AudioSource elementalshout;


	

		
		public struct Habilidades {
			public const String habilidadAire = "HabilidadAire";
			public const String habilidadTierra = "HabilidadTierra";
			public const String habilidadAgua = "HabilidadAgua";
			public const String habilidadFuego = "HabilidadFuego";
		}

		public String habilidadSeleccionada = Habilidades.habilidadAire; //por ninguna razon en especial
		public float duracionHabilidades = 10.0f;
		private ElementalPoints elemPointsMan;
		public bool habFuegoActivada = false;
		private float habilidadCntr = 0.0f;
		public float costeHabilidadAire = 10f;
		public float costeHabilidadTierra = 10f;
		public float costeHabilidadAgua = 10f;
		public float costeHabilidadFuego = 10f;
		public float velInicParticulasHabDesact = 0.03f;
		public float velInicParticulasHabAct = 0.69f;
		public Image estadoAire;
		public Image estadoTierra;
		public Image estadoAgua;
		public Image estadoFuego;
		private HealthBar vidaJugador;


        private void Start()
        {
			noCheck.Add (0);
			noCheck.Add (3);
			noCheck.Add (6);
			checkpoint = StaticVariables.sessionCheckpoint;
			if (!noCheck.Contains(checkpoint)) {
				gameObject.transform.position = new Vector3 (PlayerPrefs.GetFloat ("checkx" + checkpoint),
					                                             PlayerPrefs.GetFloat ("checky" + checkpoint),
					                                             PlayerPrefs.GetFloat ("checkz" + checkpoint));
			}
            
			anim = GetComponent<Animator>();
			// get the transform of the main camera
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.");
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

			vidaJugador = GetComponent<HealthBar>();
			elemPointsMan = GetComponent<ElementalPoints>();

            // get the third person character ( this should never be null due to require component )
			m_Character = GetComponent<ThirdPersonCharacter>();
        }


        private void Update()
        {
			manageAttack();
			manageHabilidades();

            if (!m_Jump)
            {
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }

		
			
		}
		/*void OnTriggerEnter(Collider col) //HASTA QUE LA BOLE PEGUE, NO ACTIVAR
		{
			waterattack.pitch = UnityEngine.Random.Range(0.75f, 1.3f);
			waterattack.Play ();
		}*/

		//retorna true si hay una habilidad activada
		public bool habilidadActivada(){
			return habilidadCntr > 0;
		}


		IEnumerator AirShoutFemale(){
				yield return new WaitForSeconds (0.8f);
				elementalshout.pitch = 1.8f;	
				elementalshout.Play ();
		}

		IEnumerator AirShoutMale(){
			yield return new WaitForSeconds (0.8f);
			elementalshout.Play ();
		}

		IEnumerator GroundShoutFemale(){
			yield return new WaitForSeconds (1.2f);
				elementalshout.pitch = 1.8f;	
				elementalshout.Play ();
		}
		
		IEnumerator GroundShoutMale(){
				yield return new WaitForSeconds (1.2f);
				elementalshout.Play ();
		}

		IEnumerator WaterShoutFemale(){
				yield return new WaitForSeconds (0.8f);
				elementalshout.pitch = 1.8f;	
				elementalshout.Play ();
		}
		
		IEnumerator WaterShoutMale(){
				yield return new WaitForSeconds (0.8f);
				elementalshout.Play ();
		}

		IEnumerator FireShoutFemale(){
				yield return new WaitForSeconds (0.8f);
				elementalshout.pitch = 1.8f;	
				elementalshout.Play ();
		}
		
		IEnumerator FireShoutMale(){
				yield return new WaitForSeconds (0.8f);
				elementalshout.Play ();
		}



		private void manageHabilidades(){
			//aqui seteamos la habilidad seleccionada si es que el contador de habilidad ES 0.0f;
			//Osea que si una habilidad esta ejecutandose no se puede cambiar a otra habilidad
			if(habilidadCntr <= 0.0f){
				if(Input.GetKeyDown(KeyCode.Alpha1)){
					habilidadSeleccionada = Habilidades.habilidadAire;
					abilityParticles.startColor = Color.cyan;
				}else if(Input.GetKeyDown(KeyCode.Alpha2)){
					habilidadSeleccionada = Habilidades.habilidadTierra;
					abilityParticles.startColor = Color.grey;
				}else if(Input.GetKeyDown(KeyCode.Alpha3)){
					habilidadSeleccionada = Habilidades.habilidadAgua;
					abilityParticles.startColor = Color.blue;
				}else if(Input.GetKeyDown(KeyCode.Alpha4)){
					habilidadSeleccionada = Habilidades.habilidadFuego;
					abilityParticles.startColor = Color.red;
				} 
			}

			//ahora manejamos el sistema de particulas para que solo este activo cuando hay suficientes puntos elementales para activar
			//la habilidad o si la habilidad esta actualmente activada (en este caso aumentamos el nivel de emision de particulas)

			//primero nos fijamos si no hay ninguna habilidad activada. 
			if(habilidadCntr == 0f){
				//como no hay habilidad activada entonces disminuimos la rata de emision de particulas del 
				//sistema de particulas
				abilityParticles.startSpeed = velInicParticulasHabDesact;

				//Si no hay ninguna habilidad activada entonces seteamos si el sistema de particulas esta activo dependiendo
				//de la cantidad de puntos elementales y la habilidad que se tenga equipada en el momento
				switch(habilidadSeleccionada){
				case Habilidades.habilidadAire:
					if(elemPointsMan.getAire() < costeHabilidadAire){
						abilityParticles.enableEmission = false;
					}else{
						abilityParticles.enableEmission = true;
					}
					break;
				case Habilidades.habilidadTierra:
					if(elemPointsMan.getTierra() < costeHabilidadTierra){
						abilityParticles.enableEmission = false;
					}else{
						abilityParticles.enableEmission = true;
					}
					break;
				case Habilidades.habilidadAgua:
					if(elemPointsMan.getAgua() < costeHabilidadAgua){
						abilityParticles.enableEmission = false;
					}else{
						abilityParticles.enableEmission = true;
					}
					break;
				case Habilidades.habilidadFuego:
					if(elemPointsMan.getFuego() < costeHabilidadFuego){
						abilityParticles.enableEmission = false;
					}else{
						abilityParticles.enableEmission = true;
					}
					break;
					
				}
			} else {
				//si efectivamente hay una habilidad activada entonces aumentamos la fuerza de emision del 
				//sistema de particulas
				abilityParticles.startSpeed = velInicParticulasHabAct;
			}

			//ahora aqui manejamos las animaciones respectivas
			//si click derecho y el personaje esta en el piso y no tiene ninguna otra habilidad ejecutandose
			if (Input.GetMouseButtonDown(1) && m_Character.m_IsGrounded && habilidadCntr <= 0.0f){
				//para mack -> poner gritos
				switch(habilidadSeleccionada){
				case Habilidades.habilidadAire:
					if(elemPointsMan.getAire() >= costeHabilidadAire){
						habilidadCntr = duracionHabilidades; 
						elemPointsMan.setAire(elemPointsMan.getAire() - costeHabilidadAire);
						anim.SetBool(habilidadSeleccionada, true);
						aircast.Play ();
						airready.loop = true;
						airready.Play ();
					if (StaticVariables.playerGenre == "female") {
						StartCoroutine(AirShoutFemale());
					} else {
						StartCoroutine(AirShoutMale());
					}
					}
					break;
				case Habilidades.habilidadTierra:
					if(elemPointsMan.getTierra() >= costeHabilidadTierra){
						habilidadCntr = duracionHabilidades;
						elemPointsMan.setTierra(elemPointsMan.getTierra() - costeHabilidadTierra);
						anim.SetBool(habilidadSeleccionada, true);
						earthcast.Play ();
						earthready.loop = true;
						earthready.Play ();
						if (StaticVariables.playerGenre == "female") {
							StartCoroutine(GroundShoutFemale());
						} else {
							StartCoroutine(GroundShoutMale());
						}
					}
					break;
				case Habilidades.habilidadAgua:
					if(elemPointsMan.getAgua() >= costeHabilidadAgua){
						habilidadCntr = duracionHabilidades;
						elemPointsMan.setAgua(elemPointsMan.getAgua() - costeHabilidadAgua);
						anim.SetBool(habilidadSeleccionada, true);	
						watercast.Play ();
						waterready.loop = true;
						waterready.Play ();
						if (StaticVariables.playerGenre == "female") {
							StartCoroutine(WaterShoutFemale());
						} else {
							StartCoroutine(WaterShoutMale());
						}
					}
					break;
				case Habilidades.habilidadFuego:
					if(elemPointsMan.getFuego() >= costeHabilidadFuego){
						habilidadCntr = duracionHabilidades;
						elemPointsMan.setFuego(elemPointsMan.getFuego() - costeHabilidadFuego);
						anim.SetBool(habilidadSeleccionada, true);
						firecast.Play ();
						firecharge.loop = true;
						firecharge.Play ();
						if (StaticVariables.playerGenre == "female") {
							StartCoroutine(FireShoutFemale());
						} else {
							StartCoroutine(FireShoutMale());
						}
					}
					break;

				}
			}else{
				anim.SetBool(habilidadSeleccionada, false);

			}

			//aqui manejamos lo que es precisamente habilidad por habilidad
			//Pero primero ejecutamos la instruccion q es comun a todas las habilidades
			habilidadCntr = (habilidadCntr - Time.deltaTime) < 0.0f ? 0.0f : habilidadCntr - Time.deltaTime;
			if(habilidadSeleccionada == Habilidades.habilidadAire) manageAbAir();
			if(habilidadSeleccionada == Habilidades.habilidadTierra) manageAbTierra();
			if(habilidadSeleccionada == Habilidades.habilidadAgua) manageAbAgua();
			if(habilidadSeleccionada == Habilidades.habilidadFuego) manageAbFuego();
		}

		//############################################################################
		//metodos para manejar cada una de las habilidades
		private void manageAbAir(){
			if(habilidadCntr > 0.0f){
				if(StaticVariables.playerGenre == "female")
				{
					m_Character.m_JumpPower = 8.0f;
					m_Character.m_MoveSpeedMultiplier = 1.02f;
					m_Character.m_AnimSpeedMultiplier = 1.5f;
				}
				else
				{
					m_Character.m_JumpPower = 8.0f;
					m_Character.m_MoveSpeedMultiplier = 0.62f;
					m_Character.m_AnimSpeedMultiplier = 1.5f;
				}
				//estadoAire.enabled = true;
			}else{
				if(StaticVariables.playerGenre == "female")
				{
					m_Character.m_JumpPower = 6.0f;
					m_Character.m_MoveSpeedMultiplier = 0.82f;
					m_Character.m_AnimSpeedMultiplier = 1.0f;
				}
				else
				{
					m_Character.m_JumpPower = 6.0f;
					m_Character.m_MoveSpeedMultiplier = 0.42f;
					m_Character.m_AnimSpeedMultiplier = 1.0f;
				}
				//estadoAire.enabled = false;
				airready.loop = false;
				airready.Stop();
			}
		}
		private void manageAbTierra(){
			if(habilidadCntr > 0.0f){
				vidaJugador.habilidadTierraAct = true;
				//estadoTierra.enabled = true;
			}else{
				vidaJugador.habilidadTierraAct = false;
				//estadoTierra.enabled = false;
				earthready.loop = false;
				earthready.Stop();
			}
		}
		private void manageAbAgua(){
			if(habilidadCntr > 0.0f){
				vidaJugador.setVida(vidaJugador.getVida() + 16f*Time.deltaTime); //habria que ver si esto realmente es 30f u otro numero
				//estadoAgua.enabled = true;
			}else{
				//estadoAgua.enabled = false;
				watercast.loop = false;
				waterready.Stop();
			}
		}
		private void manageAbFuego(){
			if(habilidadCntr > 0.0f){
				habFuegoActivada = true;
				//estadoFuego.enabled = true;
			}else{
				habFuegoActivada = false;
				//estadoFuego.enabled = false;
				firecharge.loop = false;
				firecharge.Stop();
			}
		}
		//####################################################################################

		private void manageAttack(){
			//esto puede ser puesto en mucho menos codigo, pero asi se entiende mejor.
			//si click izquierdo
			if (Input.GetMouseButtonDown(0) || 
			    Input.GetMouseButtonUp(0)){
				attacking = true;
				dampingTAttack = 0.2f;
			}else{
				attacking = false;
			}

			if(dampingTAttack != 0.0f){
				attacking = true;
			}

			if(attacking){
				attackswing.Play ();
				anim.SetBool("pAttacking", true);
				dampingTAttack = (dampingTAttack - Time.deltaTime) < 0.0f ? 0.0f : (dampingTAttack - Time.deltaTime); 
			}else{
				anim.SetBool("pAttacking", false);
			}
		}

        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
			float v = CrossPlatformInputManager.GetAxis("Vertical");
            float h = CrossPlatformInputManager.GetAxis("Horizontal");

			//para aplicar rotacion con el mouse entonces tenemos que encontrarnos 'idle' tanto en el
			//eje x como en el eje y.
			if(h == 0 && v == 0 ){
				h = CrossPlatformInputManager.GetAxis("Mouse X");
				// wrap values to avoid springing quickly the wrong way from positive to negative
				if (m_TargetAngles.y > 180)
				{
					m_TargetAngles.y -= 360;
					m_FollowAngles.y -= 360;
				}
				if (m_TargetAngles.x > 180)
				{
					m_TargetAngles.x -= 360;
					m_FollowAngles.x -= 360;
				}
				if (m_TargetAngles.y < -180)
				{
					m_TargetAngles.y += 360;
					m_FollowAngles.y += 360;
				}
				if (m_TargetAngles.x < -180)
				{
					m_TargetAngles.x += 360;
					m_FollowAngles.x += 360;
				}
				m_TargetAngles.y += h*rotationSpeed;
				
				
				// smoothly interpolate current values to target angles
				m_FollowAngles = Vector3.SmoothDamp(m_FollowAngles, m_TargetAngles, ref m_FollowVelocity, 0.2f);
				

				transform.localRotation = new Quaternion(); 
			}

            bool crouch = Input.GetKey(KeyCode.C);

            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v*m_CamForward + h*m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v*Vector3.forward + h*Vector3.right;
            }
#if !MOBILE_INPUT
			// walk speed multiplier
	        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

            // pass all parameters to the character control script
            m_Character.Move(m_Move, crouch, m_Jump);
            m_Jump = false;
        }

    }
}
