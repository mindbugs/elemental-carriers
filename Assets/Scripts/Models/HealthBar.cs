using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.Utility;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
	
	public GUIStyle healthBar;
	public Texture2D imageBackground;
	public Texture2D imageFront;
	private string stringNamePlayer;
	public float playerEnergy = 256f;
	//public GameObject jugador;
	public float deadSeconds = 5f;
	private Animator anim;
	public bool start;
    public ElementalPoints elementalPoints;
	BossAtack boosAtack;
	private float dampingTDamage = 0.0f;
	public float healthmaxim = 256f; //tentativamente
	ThirdPersonUserControl mCharacter;
	[HideInInspector] public bool habilidadTierraAct = false;  
	public Font venus; // Set venus font for name
	public Image imgBlood;

	private Image deathCover;
	public AudioSource attackplayerimpact;

	void Start (){
		mCharacter = GetComponent<ThirdPersonUserControl>();
		anim = GetComponent<Animator> ();
		//boosAtack = GameObject.Find ("Boss").GetComponent<BossAtack> ();
		stringNamePlayer = StaticVariables.playerName;
		elementalPoints = GetComponent<ElementalPoints>();
		deathCover = GameObject.Find("DeathCover").GetComponent<Image>();
		
	}


	void FixedUpdate(){
		Color col = imgBlood.color;
		if(col.a < 0){
			col.a = 0;
			imgBlood.color = col;
		}else if(col.a > 0){
			col.a = col.a - 0.2f*Time.deltaTime;
			imgBlood.color = col;
		}
	}


	void Update() {

		if (playerEnergy <= 0) {
			if(!anim.GetBool("IsDead")){ //si es la primera vez q se entra aqui entonces quitamos movimiento al personaje
				GetComponentInParent<SimpleMouseRotator>().enabled = false;
				GetComponent<ThirdPersonUserControl>().enabled = false;
			}
			anim.SetBool ("IsDead", true);
			deadSeconds -= Time.deltaTime;

			Color nCol = deathCover.color;
			nCol.a += 0.2f * Time.deltaTime;
			deathCover.color = nCol;

		}
		if (deadSeconds <= 0) {
			elementalPoints.PerdidaDePuntosElementales ();
			Application.LoadLevel (Application.loadedLevelName);
		}
	}

	void OnGUI() {
		GUI.color = Color.white;
		GUI.skin.font = venus;

		GUI.BeginGroup(new Rect(10, 10, 259, 37));
		GUI.Box(new Rect(0, 0, 259, 37), imageBackground,healthBar);

		GUI.BeginGroup(new Rect(4, 4, playerEnergy, 32));
		GUI.Box(new Rect(0, 0, 252, 28), imageFront,healthBar);

		GUI.EndGroup();
		GUI.EndGroup();

		GUI.BeginGroup(new Rect(10, 10, 256, 32));
		GUI.Label(new Rect(5,7,256,32), stringNamePlayer);

		GUI.EndGroup();
	}
	
	public void DamageLife(string elemento) {
		float damageMod = 1.0f; //100%
		float dannoExtra = 0.0f;

		if(habilidadTierraAct){
			damageMod = 0.2f; //pega solo el 20% del danno.
		}

		if(debilConRespectoaHabil(elemento, mCharacter.habilidadSeleccionada)){
			dannoExtra = 10.0f;
		}

		if(habilidadTierraAct){
			damageMod = 0.2f; //pega solo el 20% del danno.
		}
		playerEnergy = playerEnergy - (8.52f + dannoExtra)*damageMod < 0 ? 0 : playerEnergy - (8.52f + dannoExtra)*damageMod;
	}

	public float getVida(){
		return playerEnergy;
	}


		
	private bool debilConRespectoaHabil(string ataque, string equipado){
		if(ataque == ThirdPersonUserControl.Habilidades.habilidadAgua.ToString() && equipado == ThirdPersonUserControl.Habilidades.habilidadFuego.ToString())
			return true;
		else if(ataque == ThirdPersonUserControl.Habilidades.habilidadFuego.ToString() && equipado == ThirdPersonUserControl.Habilidades.habilidadTierra.ToString())
			return true;
		else if(ataque == ThirdPersonUserControl.Habilidades.habilidadTierra.ToString() && equipado == ThirdPersonUserControl.Habilidades.habilidadAire.ToString())
			return true;
		else if(ataque == ThirdPersonUserControl.Habilidades.habilidadAire.ToString() && equipado == ThirdPersonUserControl.Habilidades.habilidadAgua.ToString())
			return true;
		else
			return false;
	}

	public void setVida(float nuevoValor){
		playerEnergy = (nuevoValor > healthmaxim) ? healthmaxim : ((nuevoValor < 0) ? 0 : nuevoValor); // no queremos nunca que la vida sea mayor a lo maximo ni menor a lo minimo
	}

	public void DamageAnim (){
	
		attackplayerimpact.Play ();
		Color col = imgBlood.color;
		//si tiene la habilidad de tierra activada es como un escudo asi que el feedback de la imagen de blood se ve menos
		if(mCharacter.habilidadSeleccionada == ThirdPersonUserControl.Habilidades.habilidadTierra.ToString() && mCharacter.habilidadActivada()){
			col.a = 0.2f;
		}else{
			col.a = 0.3921569f;
		}
		imgBlood.color = col;
	}
	/*
	public  void OnCollisionStay (Collision obj){
		//GameObject.Find("PlayerModel").GetComponent<BarraVida> ();
		if (obj.gameObject.name == "Boss" && boosAtack.attacking == true) { //Aqui se pone un && para saber si el enemigo principal esta atacando también 
			//print ("El boos ataco quitar vida al ersonaje principal");
			energiaJugador -= 150 * Time.deltaTime;
			//OnCollisionEnter (Collision obj);
		}
	}
	*/
	/*
	public  void OnCollisionStay (Collision obj){
		if (obj.gameObject.name == "skull_axe") { 
			print ("el boss me ataco ;( !!!");
		}
	}*/
}
