﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BernaScript : MonoBehaviour {

	public GameObject blockFightZone;
	public Text objectiveOne;
	Color objOriginaColor;
	float delayTime = 5f;

	void Start()
	{
		objOriginaColor = objectiveOne.color;
	}

	void Update()
	{
		if(blockFightZone.activeSelf) 
		{
			delayTime -= Time.deltaTime;
			if(delayTime <= 0) ChangeObjectiveOne();
		}
		if(StaticVariables.enemyCount >= 1) blockFightZone.SetActive (false);
	}

	void OnTriggerEnter() 
	{
		blockFightZone.SetActive (true);
		objectiveOne.color = Color.green;
	}

	void ChangeObjectiveOne()
	{
		objectiveOne.color = objOriginaColor;
		objectiveOne.text = "- Kill the boss";
	}

}
