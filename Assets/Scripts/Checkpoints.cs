﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Checkpoints : MonoBehaviour {

	public int checkpointnum;
	public Text checkpointText;
	bool entrance;
	float fadeSpeed = 0.05f;

	void Start () 
	{
		// Setting checkpoint text
		checkpointText.color = Color.clear;
	}
	
	// Update is called once per frame
	void Update () 
	{
		ColorChange();
	}

	void OnTriggerEnter(Collider col)
	{
		StaticVariables.sessionCheckpoint = checkpointnum; // Set checkpoint number
		entrance = true; // Let know when player is inside checkpoint zone
	}
	
	void OnTriggerExit(Collider col)
	{
		entrance = false; // Let know when player leave checkpoint zone
	}
	
	void ColorChange() 
	{
		if(entrance)
		{
			// Make checkpoint text visible
			checkpointText.color = Color.Lerp(checkpointText.color, Color.white, fadeSpeed + Time.deltaTime);
		}
		
		if(!entrance)
		{
			// Hide checkpoint text
			checkpointText.color = Color.Lerp(checkpointText.color, Color.clear, fadeSpeed + Time.deltaTime);
		}
	}

}
