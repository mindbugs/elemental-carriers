﻿using UnityEngine;
using System.Collections;

public class MoveToTarget : MonoBehaviour {
	public float movingSpeed = 1f;
	public Transform target;

	private Transform transform;

	void Start () {
		transform = gameObject.transform;
		GetComponent<Rigidbody>().interpolation = RigidbodyInterpolation.Interpolate;
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position != target.position){
			transform.position = Vector3.MoveTowards(transform.position,  target.position, movingSpeed * Time.deltaTime);
		}
	}
}
