﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
public class zoomMod : MonoBehaviour {

	private Camera cam;
	public float maxZoom = 36.0f;
	public float minZoom = 76.0f;
	public float zoomSpeed = 1f;
	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (CrossPlatformInputManager.GetAxis("Mouse ScrollWheel") < 0 && cam.fieldOfView < minZoom) 
		{	
			cam.fieldOfView += zoomSpeed;

		}
		if (CrossPlatformInputManager.GetAxis("Mouse ScrollWheel") > 0 && cam.fieldOfView > maxZoom)
		{
			cam.fieldOfView -= zoomSpeed;
		}
	}
}
