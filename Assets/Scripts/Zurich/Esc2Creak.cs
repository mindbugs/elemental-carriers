﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Esc2Creak : MonoBehaviour {
	public AudioSource esc2creak;
	// Use this for initialization
	void Start () {
		AudioSource esc2creak = GetComponent<AudioSource> ();
		StartCoroutine(LoopCreak (esc2creak, 15));
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	IEnumerator LoopCreak(AudioSource audioSource, int delay){
		while(true){ 
			yield return new WaitForSeconds(delay);
			audioSource.Play();
		}
	}
}
