﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Esc1Creak : MonoBehaviour {


	public AudioSource esc1creak;
	// Use this for initialization
	void Start () {
		AudioSource esc1creak = GetComponent<AudioSource> ();
		StartCoroutine(LoopCreak (esc1creak, 15));
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator LoopCreak(AudioSource audioSource, int delay){
		while(true){ 
			yield return new WaitForSeconds(delay);
			audioSource.Play();
		}
	}
}
