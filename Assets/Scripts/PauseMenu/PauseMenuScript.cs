﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;



public class PauseMenuScript : MonoBehaviour {

	public GameObject pauseMenu;
	public GameObject mainMenuConfirm;
	public GameObject backgroundPM;
	public GameObject confirmSave;
	public Button resume;
	public Button save;
	public Button mainMenu;
	public Button menu;
	public Transform player;
	private bool paused;
    public int session;
	public int checkpoint;
	PlayerPrefsX prefs;
	public GameObject checkPointOne;
	public GameObject checkPointTwo;
	public GameObject checkPointFour;
	public GameObject checkPointFive;



	public GameObject enemigosPreCheckPointOne;
	public GameObject enemigosPreCheckPointTwo;
	public GameObject enemigosPreCheckPointFour;
	public GameObject enemigosPreCheckPointFive;

	public void MenuPress()
	{
		paused = true;
		menu.enabled = !menu.enabled;
	}

	public void ResumePress()
	{
		paused = false;
		menu.enabled = true;
	}

    public void SavePress()
    {
		session = StaticVariables.sessionPlaying;
		checkpoint = StaticVariables.sessionCheckpoint;

		if (checkpoint == 1)
		{
			PlayerPrefs.SetFloat("checkx" + checkpoint, checkPointOne.transform.position.x);
			PlayerPrefs.SetFloat("checky" + checkpoint, checkPointOne.transform.position.y);
			PlayerPrefs.SetFloat("checkz" + checkpoint, checkPointOne.transform.position.z);
		}
		if (checkpoint == 2)
		{
			PlayerPrefs.SetFloat("checkx" + checkpoint, checkPointTwo.transform.position.x);
			PlayerPrefs.SetFloat("checky" + checkpoint, checkPointTwo.transform.position.y);
			PlayerPrefs.SetFloat("checkz" + checkpoint, checkPointTwo.transform.position.z);
		}
		
		if (checkpoint == 4)
		{
			PlayerPrefs.SetFloat("checkx" + checkpoint, checkPointFour.transform.position.x);
			PlayerPrefs.SetFloat("checky" + checkpoint, checkPointFour.transform.position.y);
			PlayerPrefs.SetFloat("checkz" + checkpoint, checkPointFour.transform.position.z);
		}
		
		if (checkpoint == 5)
		{
			PlayerPrefs.SetFloat("checkx" + checkpoint, checkPointFive.transform.position.x);
			PlayerPrefs.SetFloat("checky" + checkpoint, checkPointFive.transform.position.y);
			PlayerPrefs.SetFloat("checkz" + checkpoint, checkPointFive.transform.position.z);
		}

		if (session == 1)
		{
			PlayerPrefs.SetInt ("saveslot1", 1);
			PlayerPrefs.SetInt ("continue", 1);
			PlayerPrefs.SetInt ("escena1", Application.loadedLevel);
			PlayerPrefs.SetInt ("checkpoint1", checkpoint);
			PlayerPrefs.SetString ("gender1", StaticVariables.playerGenre);
			PlayerPrefs.SetString ("name1", StaticVariables.playerName);
			PlayerPrefs.SetInt ("weapon1", (int)StaticVariables.selectedWeapon);
			PlayerPrefs.SetInt ("enemies1", StaticVariables.enemyCount);
		}
		else if (session == 2)
		{
			PlayerPrefs.SetInt ("saveslot2", 1);
			PlayerPrefs.SetInt ("continue", 2);
			PlayerPrefs.SetInt ("escena2", Application.loadedLevel);
			PlayerPrefs.SetInt ("checkpoint2", checkpoint);
			PlayerPrefs.SetString ("gender2", StaticVariables.playerGenre);
			PlayerPrefs.SetString ("name2", StaticVariables.playerName); 
			PlayerPrefs.SetInt ("weapon2", (int)StaticVariables.selectedWeapon);
			PlayerPrefs.SetInt ("enemies2", StaticVariables.enemyCount);
		}
		else
		{
			PlayerPrefs.SetInt ("saveslot3", 1);
			PlayerPrefs.SetInt ("continue", 3);
			PlayerPrefs.SetInt ("escena3", Application.loadedLevel);
			PlayerPrefs.SetInt ("checkpoint3", checkpoint);
			PlayerPrefs.SetString ("gender3", StaticVariables.playerGenre);
			PlayerPrefs.SetString ("name3", StaticVariables.playerName);
			PlayerPrefs.SetInt ("weapon3", (int)StaticVariables.selectedWeapon);
			PlayerPrefs.SetInt ("enemies3", StaticVariables.enemyCount);
		}

		PlayerPrefs.Save ();
		confirmSave.SetActive(true);
	}


	public void MainMenuPress()
	{
		mainMenuConfirm.SetActive(true);
		resume.enabled = false;
		save.enabled = false;
		mainMenu.enabled = false;
		menu.enabled = false;
	}

	public void NoPress()
	{
		mainMenuConfirm.SetActive(false);
		resume.enabled = true;
		save.enabled = true;
		mainMenu.enabled = true;
	}

	public void MainMenu()
	{
		Application.LoadLevel (1);
	}

	void Start () 
	{


		//Activa los enemigos apropiados en el escenario
		checkpoint = StaticVariables.sessionCheckpoint;
		switch(checkpoint){
		case 1:
			enemigosPreCheckPointOne.SetActive(false);
			break;
		case 2:
			enemigosPreCheckPointOne.SetActive(false);
			enemigosPreCheckPointTwo.SetActive(false);
			break;
		case 4:
			enemigosPreCheckPointFour.SetActive(false);
			break;
		case 5:
			enemigosPreCheckPointFour.SetActive(false);
			enemigosPreCheckPointFive.SetActive(false);
			break;
		}
	}

	void Update ()
	{
		if (paused)
		{
			pauseMenu.SetActive (true);
			backgroundPM.SetActive (true);
			Time.timeScale = 0f;
			menu.enabled = false;
		}
		else
		{
			pauseMenu.SetActive (false);
			backgroundPM.SetActive (false);
			Time.timeScale = 1f;
			menu.enabled = true;
		}
		// Open and close pause menu with 'Esc' key
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			paused = !paused;
			NoPress();
			OkPress();
		}
	}

	public void OkPress()
	{
		confirmSave.SetActive(false);
	}

}


