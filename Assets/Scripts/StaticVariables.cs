﻿using UnityEngine;
using System.Collections;

public class StaticVariables : MonoBehaviour {

	static public int levelToGo = 0;
	static public int sessionPlaying = 1;
	static public int sessionCheckpoint = 0;
	static public string playerName = "";
	static public string playerGenre = "male";
	static public int storyScene = 0;
	static public int enemyCount = 0;
	static public bool disconnected = false;

	static public float ptsElAire = 100;
	static public float ptsElTierra = 100;
	static public float ptsElAgua = 100;
	static public float ptsElFuego = 100;

	public enum Weapon
	{
		Axe,		// 0
		Hammer,		// 1
		Sword,		// 2
		Mace,		// 3
		None
	}

	static public Weapon selectedWeapon = Weapon.Sword; //por el momento es sword por defecto

}
