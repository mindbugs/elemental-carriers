﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LoadScreenScript : MonoBehaviour {

	public Image progressBar;
	public Image hintImage;
	public Sprite controls;
	public Sprite controls1;
	public Sprite controls2;
	public Sprite health;
	List<Sprite> sprites = new List<Sprite>(); // List of hints images

	public GameObject startButton;
	public Text percentage;
	public Text loading;

	AsyncOperation asyncLoad;

	public GameObject leftArrow;
	public GameObject rightArrow;

	private int hintNumber = 0;

	//Since Unity LoadLevelAsync doesn't work that well we going to set a loading
	//manually for visual while the Asyng works
	private float maxStrenght = 1f;
	private float recoveryRate = 0.2f;

	void Start ()
	{
		sprites.Add(controls);
		sprites.Add(controls1);
		sprites.Add(controls2);
		sprites.Add(health);
		//sprites = Resources.LoadAll<Sprite>("Images/LoadScreen/Hints"); 
		hintImage.overrideSprite = sprites[0]; // Random.Range(0, sprites.Count)
		Time.timeScale = 1.0f;
		StartCoroutine (LoadScene());
	}

	//Start loading the next level
	IEnumerator LoadScene ()
	{
		asyncLoad = Application.LoadLevelAsync (StaticVariables.levelToGo);
		asyncLoad.allowSceneActivation = false;
		yield return asyncLoad;
	}

	// Update is called once per frame
	void Update ()
	{
		// Fills progress bar while loads the next level
		progressBar.fillAmount = Mathf.MoveTowards(progressBar.fillAmount, maxStrenght, recoveryRate * Time.deltaTime);
		ChangeValues ();

		// Hide unhide arrows
		HideArrow ();
	}
	
	void ChangeValues ()
	{
		// Percentage text update to progress bar
		percentage.text = Mathf.Floor(progressBar.fillAmount * 100) + "%";

		// When loading is done the loading and percentage text hides
		// and the start button unhide
		if (percentage.text.Equals ("100%"))
		{
			progressBar.fillAmount = 1.0f;
			Color alpha = percentage.color;
			alpha.a = 0;
			percentage.color = alpha;
			loading.color = alpha;
			startButton.SetActive(true);
		}
	}

	void HideArrow ()
	{
		if (hintNumber == 0)
		{
			leftArrow.SetActive (false);
		}
		else
		{
			leftArrow.SetActive (true);
		}
		if (hintNumber == sprites.Count - 1)
		{
			rightArrow.SetActive(false);
		}
		else
		{
			rightArrow.SetActive(true);
		}
	}

	public void StartLevel ()
	{
		// On button click start the level
		asyncLoad.allowSceneActivation = true;
	}

	public void LeftArrow ()
	{
		hintNumber -= 1;
		ChangeHint (hintNumber);
	}

	public void RightArrow ()
	{
		hintNumber += 1;
		ChangeHint (hintNumber);
	}

	public void ChangeHint (int hint)
	{
		hintImage.overrideSprite = sprites [hint];
	}
}
