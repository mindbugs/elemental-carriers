﻿	using UnityEngine;
using System.Collections;

public class AutoDestruccionBola : MonoBehaviour {
	public float destroyTime = 5.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (destroyTime <= 0.0f) {
			Destroy (gameObject);
		} else {
			destroyTime -= Time.deltaTime;
		}
	}
}
