﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class MainMenuScript : MonoBehaviour {
	
	// All containers are disabled
	public GameObject contenedorAbout;
	public GameObject contenedorOptions;
	public GameObject contenedorLoadGame;
	public GameObject contenedorNewGame;
	public GameObject contenedorSelectCharacter;
	public GameObject contenedorSelectMultiplayer;
	public Button btnContinue;

	// Slot files text
	public Text txtGame1;
	public Text txtGame2;
	public Text txtGame3;

	public GameObject btnGame1;
	public GameObject btnGame2;
	public GameObject btnGame3;
	public Button btn1inac;
	public Button btn2inac;
	public Button btn3inac;
	public Image shaderImage;

	private bool newgame = false; // para ver si el contenedor de load game es para new game o load game
	int sceneNumber; // Set the scene number to load

	// Player selection
	public InputField charName;
	public Text validate;
	public Image femaleImage;
	public Image maleImage;
	public Sprite female;
	public Sprite femaleSelected;
	public Sprite male;
	public Sprite maleSelected;
	public Image maceImage;
	public Sprite mace;
	public Sprite maceSelected;
	public Image hammerImage;
	public Sprite hammer;
	public Sprite hammerSelected;
	public Image swordImage;
	public Sprite sword;
	public Sprite swordSelected;
	public Image axeImage;
	public Sprite axe;
	public Sprite axeSelected;

	// MultiplayerSelection
	public InputField multiCharName;
	public Text multiValidate;
	public Image multiFemaleImage;
	public Image multiMaleImage;
	public Image multiMaceImage;
	public Image multiHammerImage;
	public Image multiSwordImage;
	public Image multiAxeImage;

	// Use this for initialization
	void Start () {
		sceneNumber = Application.loadedLevel + 3;

		//PlayerPrefs.DeleteAll (); 

		//Activar o desactivar continue
		if ((PlayerPrefs.GetInt ("continue") == 1)||(PlayerPrefs.GetInt ("continue") == 2)||(PlayerPrefs.GetInt ("continue") == 3)){
			btnContinue.interactable = true;
		} else {
			btnContinue.interactable = false;
		}

		if(StaticVariables.disconnected) 
		{
			contenedorSelectMultiplayer.SetActive(true);
			StaticVariables.disconnected = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		fadeIn();
	}

	private void fadeIn(){
		if(shaderImage.color.a >= 0){
			Color shaderCol = shaderImage.color;		
			shaderCol.a -= 0.0251f;
			if(shaderCol.a <= 0){
				shaderCol.a = 0;
			}
			shaderImage.color = shaderCol;
			if(shaderImage.color.a <= 0){
				shaderImage.gameObject.SetActive(false);
			}
		}
	}

	public void StartNewGame(bool close){
		if(!otherPanelOpen() || close){
			contenedorNewGame.SetActive(!contenedorNewGame.activeSelf);
			newgame = true;
		}
	}


	public void LoadGameClose(bool close){
		if (!otherPanelOpen () || close) {
			contenedorLoadGame.SetActive (!contenedorLoadGame.activeSelf);
			newgame = false;
		}
	}

	public void LoadGame(bool close){
		if (!otherPanelOpen () || close) {
			contenedorLoadGame.SetActive (!contenedorLoadGame.activeSelf);
		}

		//Cambiar texto de botones
		//Game1
		if (PlayerPrefs.GetInt ("saveslot1") == 1) {
			txtGame1.text = "Game 1";
		} else {
			txtGame1.text = "Empty";
		}

		//Game2
		if (PlayerPrefs.GetInt ("saveslot2") == 1) {
			txtGame2.text = "Game 2";
		} else {
			txtGame2.text = "Empty";
		}
		//Game3
		if (PlayerPrefs.GetInt ("saveslot3") == 1) {
			txtGame3.text = "Game 3";
		} else {
			txtGame3.text = "Empty";
		}
	}
	
	public void Options(bool close){
		if(!otherPanelOpen() || close){
			contenedorOptions.SetActive(!contenedorOptions.activeSelf);
		}
	}
	
	public void ToggleAbout(bool close){
		if(!otherPanelOpen() || close){
			contenedorAbout.SetActive(!contenedorAbout.activeSelf);
		}
	}
	
	public void SetGenre(string gen) 
	{
		StaticVariables.playerGenre = gen;
		if (gen == "male") 
		{
			maleImage.overrideSprite = maleSelected;
			femaleImage.overrideSprite = female;
			multiMaleImage.overrideSprite = maleSelected;
			multiFemaleImage.overrideSprite = female;
		}
		else
		{
			femaleImage.overrideSprite = femaleSelected;
			maleImage.overrideSprite = male;
			multiFemaleImage.overrideSprite = femaleSelected;
			multiMaleImage.overrideSprite = male;
		}
	}

	public void SetWeapon(int weapon)
	{
		switch (weapon)
		{
		case 1:
			StaticVariables.selectedWeapon = StaticVariables.Weapon.Mace;
			maceImage.overrideSprite = maceSelected;
			hammerImage.overrideSprite = hammer;
			swordImage.overrideSprite = sword;
			axeImage.overrideSprite = axe;
			break;
		case 2:
			StaticVariables.selectedWeapon = StaticVariables.Weapon.Hammer;
			maceImage.overrideSprite = mace;
			hammerImage.overrideSprite = hammerSelected;
			swordImage.overrideSprite = sword;
			axeImage.overrideSprite = axe;
			break;
		case 3:
			StaticVariables.selectedWeapon = StaticVariables.Weapon.Sword;
			maceImage.overrideSprite = mace;
			hammerImage.overrideSprite = hammer;
			swordImage.overrideSprite = swordSelected;
			axeImage.overrideSprite = axe;
			break;
		case 4:
			StaticVariables.selectedWeapon = StaticVariables.Weapon.Axe;
			maceImage.overrideSprite = mace;
			hammerImage.overrideSprite = hammer;
			swordImage.overrideSprite = sword;
			axeImage.overrideSprite = axeSelected;
			break;
		case 5:
			StaticVariables.selectedWeapon = StaticVariables.Weapon.Mace;
			multiMaceImage.overrideSprite = maceSelected;
			multiHammerImage.overrideSprite = hammer;
			multiSwordImage.overrideSprite = sword;
			multiAxeImage.overrideSprite = axe;
			break;
		case 6:
			StaticVariables.selectedWeapon = StaticVariables.Weapon.Hammer;
			multiMaceImage.overrideSprite = mace;
			multiHammerImage.overrideSprite = hammerSelected;
			multiSwordImage.overrideSprite = sword;
			multiAxeImage.overrideSprite = axe;
			break;
		case 7:
			StaticVariables.selectedWeapon = StaticVariables.Weapon.Sword;
			multiMaceImage.overrideSprite = mace;
			multiHammerImage.overrideSprite = hammer;
			multiSwordImage.overrideSprite = swordSelected;
			multiAxeImage.overrideSprite = axe;
			break;
		case 8:
			StaticVariables.selectedWeapon = StaticVariables.Weapon.Axe;
			multiMaceImage.overrideSprite = mace;
			multiHammerImage.overrideSprite = hammer;
			multiSwordImage.overrideSprite = sword;
			multiAxeImage.overrideSprite = axeSelected;
			break;
		default:
			break;
		}
	}

	public void GoToLevel() 
	{
		if (charName.text.Equals ("") && StaticVariables.playerGenre.Equals ("") && StaticVariables.selectedWeapon == StaticVariables.Weapon.None) 
		{
			validate.text = "Select your character gender, weapon and name";
		} 
		else if (charName.text.Equals ("")) 
		{
			validate.text = "Type your character name";
		} 
		else if (StaticVariables.playerGenre.Equals ("")) 
		{
			validate.text = "Select your character genre";
		} 
		else if (StaticVariables.selectedWeapon == StaticVariables.Weapon.None)
		{
			validate.text = "Select your character weapon";
		}
		else 
		{
			StaticVariables.levelToGo = sceneNumber; // Set scene for loading screen
			StaticVariables.storyScene = 1; // Set Intro and Munich story
			StaticVariables.playerName = charName.text; // Set player name
			Application.LoadLevel ("Story"); // Start game story
		}
	}

	public void CloseSelectChar() {
		contenedorSelectCharacter.SetActive(false);
		ResetCharacterSelection(0);
	}

	//Open Multiplayer panel
	public void CreateMultiplayer()
	{
		contenedorSelectMultiplayer.SetActive(true);
	}

	public void CloseMultiPlayer()
	{
		contenedorSelectMultiplayer.SetActive(false);
		ResetCharacterSelection(1);
	}

	void ResetCharacterSelection(int close)
	{
		if(close == 0)
		{
			charName.text = "";
			validate.text = "";
			maceImage.overrideSprite = mace;
			hammerImage.overrideSprite = hammer;
			swordImage.overrideSprite = sword;
			axeImage.overrideSprite = axe;
		}
		else
		{
			multiCharName.text = "";
			multiValidate.text = "";
			multiMaceImage.overrideSprite = mace;
			multiHammerImage.overrideSprite = hammer;
			multiSwordImage.overrideSprite = sword;
			multiAxeImage.overrideSprite = axe;
		}
		femaleImage.overrideSprite = female;
		maleImage.overrideSprite = male;
		multiFemaleImage.overrideSprite = female;
		multiMaleImage.overrideSprite = male;
		StaticVariables.playerGenre = "";
		StaticVariables.selectedWeapon = StaticVariables.Weapon.None;
	}
	
	public void ExitApplication(){
		#if UNITY_EDITOR
		Debug.Log("Exiting Application");
		#else
		Application.Quit();
		#endif
	}

	//retorna true si hay otro panel abierto, esto con el proposito de que si hay un panel abierto no se abra otro
	//encima
	private bool otherPanelOpen(){
		return 	contenedorAbout.activeSelf || 
			contenedorOptions.activeSelf || 
			contenedorLoadGame.activeSelf;
	}
	

	public void LoadContinue(){
		if (PlayerPrefs.GetInt ("continue") == 1) {
			StaticVariables.sessionPlaying = 1;
			StaticVariables.playerGenre = PlayerPrefs.GetString ("gender1");
			StaticVariables.playerName = PlayerPrefs.GetString ("name1");
			StaticVariables.selectedWeapon = (StaticVariables.Weapon)PlayerPrefs.GetInt("weapon1");
			StaticVariables.sessionCheckpoint = PlayerPrefs.GetInt ("checkpoint1");
			StaticVariables.levelToGo = PlayerPrefs.GetInt ("escena1");
			StaticVariables.enemyCount = PlayerPrefs.GetInt ("enemies1");
		} else if (PlayerPrefs.GetInt ("continue") == 2) {
			StaticVariables.sessionPlaying = 2;
			StaticVariables.playerGenre = PlayerPrefs.GetString ("gender2");
			StaticVariables.playerName = PlayerPrefs.GetString ("name2");
			StaticVariables.selectedWeapon = (StaticVariables.Weapon)PlayerPrefs.GetInt("weapon2");
			StaticVariables.sessionCheckpoint = PlayerPrefs.GetInt ("checkpoint2");
			StaticVariables.levelToGo = PlayerPrefs.GetInt ("escena2");
			StaticVariables.enemyCount = PlayerPrefs.GetInt ("enemies2");
		} else if (PlayerPrefs.GetInt ("continue") == 3){
			StaticVariables.sessionPlaying = 3;
			StaticVariables.playerGenre = PlayerPrefs.GetString ("gender3");
			StaticVariables.playerName = PlayerPrefs.GetString ("name3");
			StaticVariables.selectedWeapon = (StaticVariables.Weapon)PlayerPrefs.GetInt("weapon3");
			StaticVariables.sessionCheckpoint = PlayerPrefs.GetInt ("checkpoint3");
			StaticVariables.levelToGo = PlayerPrefs.GetInt ("escena3");
			StaticVariables.enemyCount = PlayerPrefs.GetInt ("enemies3");
		}

		Application.LoadLevel ("LoadScreen");
	}

	public void CreateGame(int game) {
		if (newgame) {
			StaticVariables.sessionPlaying = game;
			if (game == 1){
			PlayerPrefs.SetInt("checkpoint1", 0);
			PlayerPrefs.SetInt ("escena1", Application.loadedLevel + 3);
			PlayerPrefs.SetInt ("saveslot1", 0);
			PlayerPrefs.SetInt ("continue", 0);
			}
			if (game == 2){
			PlayerPrefs.SetInt("checkpoint2", 0);
			PlayerPrefs.SetInt ("escena2", Application.loadedLevel + 3);
			PlayerPrefs.SetInt ("saveslot2", 0);
			PlayerPrefs.SetInt ("continue", 0);
			}
			if (game == 3){
			PlayerPrefs.SetInt("checkpoint3", 0);
			PlayerPrefs.SetInt ("escena3", Application.loadedLevel + 3);
			PlayerPrefs.SetInt ("saveslot3", 0);
			PlayerPrefs.SetInt ("continue", 0);
			}

			//seguir al screen de genero y nombre!!!
			contenedorSelectCharacter.SetActive (true);
			
			// Close file selection
			contenedorLoadGame.SetActive (!contenedorLoadGame.activeSelf);
		} else {
			if (game == 1 && PlayerPrefs.GetInt ("saveslot1") != 0){
				StaticVariables.sessionPlaying = 1;
				StaticVariables.sessionCheckpoint = PlayerPrefs.GetInt("checkpoint1");
				StaticVariables.levelToGo = PlayerPrefs.GetInt ("escena1");
				StaticVariables.playerGenre = PlayerPrefs.GetString ("gender1");
				StaticVariables.playerName = PlayerPrefs.GetString ("name1");
				StaticVariables.selectedWeapon = (StaticVariables.Weapon)PlayerPrefs.GetInt("weapon1");
				StaticVariables.enemyCount = PlayerPrefs.GetInt ("enemies1");
				Application.LoadLevel("LoadScreen");
			}
			if (game == 2 && PlayerPrefs.GetInt ("saveslot2") != 0){
				StaticVariables.sessionPlaying = 2;
				StaticVariables.playerGenre = PlayerPrefs.GetString ("gender2");
				StaticVariables.playerName = PlayerPrefs.GetString ("name2");
				StaticVariables.selectedWeapon = (StaticVariables.Weapon)PlayerPrefs.GetInt("weapon2");
				StaticVariables.sessionCheckpoint = PlayerPrefs.GetInt("checkpoint2");
				StaticVariables.levelToGo = PlayerPrefs.GetInt ("escena2");
				StaticVariables.enemyCount = PlayerPrefs.GetInt ("enemies2");
				Application.LoadLevel("LoadScreen");
			}
			if (game == 3 && PlayerPrefs.GetInt ("saveslot3") != 0){
				StaticVariables.sessionPlaying = 3;
				StaticVariables.playerGenre = PlayerPrefs.GetString ("gender3");
				StaticVariables.playerName = PlayerPrefs.GetString ("name3");
				StaticVariables.selectedWeapon = (StaticVariables.Weapon)PlayerPrefs.GetInt("weapon3");
				StaticVariables.sessionCheckpoint = PlayerPrefs.GetInt("checkpoint3");
				StaticVariables.levelToGo = PlayerPrefs.GetInt ("escena3");
				StaticVariables.enemyCount = PlayerPrefs.GetInt ("enemies3");
				Application.LoadLevel("LoadScreen");
			}

		}
	}

}
