﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectionInfo : MonoBehaviour {

	public GameObject tooltip;
	public Text tooltipText;

	public void ShowToolTip(int selection)
	{
		tooltip.SetActive(true);
		SetTooltipText(selection);
	}

	void SetTooltipText(int textID)
	{
		switch(textID)
		{
		case 1:
			tooltipText.text = "The female character is well known for being an extraordinary weapon wielder, given her an extra damage while attacking.";
			break;
		case 2:
			tooltipText.text = "The male character is well known for his incredible agility, given him a movement speed bonus.";
			break;
		case 3:
			tooltipText.text = "Mace greatly increase attack power, but reduces attack speed and movement.";
			break;
		case 4:
			tooltipText.text = "Hammer give a balance between attack power and movement.";
			break;
		case 5:
			tooltipText.text = "Sword give a bonus on attack speed and movement, but reduces attack power.";
			break;
		case 6:
			tooltipText.text = "Axe give a balance between attack speed and attack power.";
			break;
		default:
			break;
		}
	}

	public void HideToolTip()
	{
		tooltip.SetActive(false);
	}

}
