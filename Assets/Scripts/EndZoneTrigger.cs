﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndZoneTrigger : MonoBehaviour {

	public Text objectiveOne;
	public Text objectiveTwo;
	public int continueNum;
	public int storyNum;
	public int sceneEnemyCount;
	bool objOneCompleted = false;
	public Image fadeIn;
	bool changeLevel = false;
	float blackColor = 1.5f;

	// Checks objective one progress
	void Update()
	{
		CheckObjectiveOne();

		if(changeLevel)
		{
			if(fadeIn.color.a >= 0){
				Color shaderCol = fadeIn.color;
				shaderCol.a += 0.007f;
				fadeIn.color = shaderCol;
				if(fadeIn.color.a >= blackColor){
					Application.LoadLevel ("Story"); // Start story scene
				}
			}
		}
	}

	void CheckObjectiveOne()
	{
		if(StaticVariables.enemyCount >= sceneEnemyCount)
		{
			// Objective one completed
			objectiveOne.color = Color.green;
			objOneCompleted = true;
		}
	}

	// When player is at the ending zone the level will finish
	// and start loading the next level.
	void OnTriggerEnter () 
	{
		if(continueNum == 7)
		{
			StaticVariables.levelToGo = 1; // For now, just to see continue and go to MainMenu
		}
		else
		{
			StaticVariables.levelToGo = Application.loadedLevel + 1; // Set scene for loading screen
		}
		StaticVariables.storyScene = storyNum; // Set scene story
		objectiveTwo.color = Color.green; // Objective two completed
	}

	void OnTriggerExit () 
	{
		if(objOneCompleted)
		{
			StaticVariables.sessionCheckpoint = continueNum;
			StaticVariables.enemyCount = 0; // Reset enemy count for next scene
			changeLevel = true;
		}
	}

}
