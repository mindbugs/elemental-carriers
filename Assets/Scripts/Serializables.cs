﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

//TUTORIAL
[Serializable]
public class PlayerStatistics
{
    public int SceneID;
    public float PositionX, PositionY, PositionZ;

    public float HP;
    public float Ammo;
    public float XP;
}
