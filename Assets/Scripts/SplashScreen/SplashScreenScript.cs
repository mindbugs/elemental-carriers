﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class SplashScreenScript : MonoBehaviour {

	public Image shaderImage;
	public float maxShade = 1.5f;
	public GameObject splash1; //su estado inicial es activo
	public GameObject splash2; //su estado inicial es activo
	public AudioSource sound;

	private bool isShading = false;
	private bool isUnshading = true; //empezamos con pantalla negra y mostramos lentamente la imagen
	private bool dimVol = false;

	private bool state1 = true; //se esta trabajando con la primera imagen 
	private bool state2 = false; //se esta trabajando con la segunda imagen
	private bool state3 = false; //se esta trabajando con la segunda imagen
	private bool state4 = false; //se esta trabajando con la segunda imagen, cuando termina el shade se pasa al main menu
	


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		processStates();
		processShade();
		processSound();
	}


	void processStates(){
		if(state1 && !isUnshading){ //se trabaja con la primera imagen pero ya no se estan unshading
			shade();
			state1 = false;
			state2 = true;
		}

		if(state2 && !isShading){
			splash1.SetActive(false); //splash1 ahora es inactive, por lo tanto se muestra splash2 que esta debajo
			unshade();
			state2 = false;
			state3 = true;
		}
		if(state3 && !isUnshading){
			dimVol = true;
			state3 = false;
			state4 = true;
			shade();			
		}
		if(state4 && !isShading){
			Application.LoadLevel("MainMenu");
		}
	}


	void processShade(){
		Color shaderCol = shaderImage.color;		
		if(isShading && shaderCol.a < maxShade){
			shaderCol.a += 0.0051f;
			if(shaderCol.a >= maxShade){
				isShading = false;
				shaderCol.a = maxShade;
			}
		}
		if(isUnshading && shaderCol.a >= 0){
			shaderCol.a -= 0.0051f;
			if(shaderCol.a <= 0){
				isUnshading = false;
				shaderCol.a = 0;
			}
		}
		shaderImage.color = shaderCol;
	}

	void processSound(){
		if(dimVol){
			sound.volume -= 0.005f;
		}
	}

	public void shade(){
		this.isShading = !isUnshading ? true : false;
	}

	public void unshade(){
		this.isUnshading = !isShading ? true : false;
	}
}
