﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class GameManager_GolemSpawner : NetworkBehaviour {

	[SerializeField]GameObject golemPrefab;
	private GameObject[] golemSpawns;
	private int counter;
	private int numberOfGolems = 6;
	private int maxNumberOfGolems = 3;
	private float waveRate = 10;
	private bool isSpawnActivated = true;

	// Use this for initialization
	public override void OnStartServer () 
	{
		golemSpawns = GameObject.FindGameObjectsWithTag("GolemSpawn");
		StartCoroutine(GolemSpawner());
	}

	IEnumerator GolemSpawner()
	{
		for(;;)
		{
			yield return new WaitForSeconds(waveRate);
			GameObject[] golems = GameObject.FindGameObjectsWithTag("Golem");
			if(golems.Length < maxNumberOfGolems)
			{
				CommenceSpawn();
			}
		}
	}
	
	void CommenceSpawn()
	{
		if(isSpawnActivated)
		{
			for(int i = 0; i < maxNumberOfGolems; i++)
			{
				int randomIndex = Random.Range(0, golemSpawns.Length);
				SpawnGolems(golemSpawns[randomIndex].transform.position);
			}
		}
	}

	void SpawnGolems(Vector3 spawnPos)
	{
		counter ++;
		GameObject go = GameObject.Instantiate(golemPrefab, spawnPos, Quaternion.identity) as GameObject;
		go.GetComponent<GolemID>().golemID = "Golem " + counter;
		NetworkServer.Spawn(go);
	}
	
}
