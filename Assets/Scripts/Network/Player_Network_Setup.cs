﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_Network_Setup : NetworkBehaviour {

	[SerializeField]Camera thirdPersonCam;
	[SerializeField]AudioListener audio;
	
	// Use this for initialization
	public override void OnStartLocalPlayer () 
	{
		GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().enabled = true;
		GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>().enabled = true;
		thirdPersonCam.enabled = true;
		audio.enabled = true;
		(GetComponent("SimpleMouseRotator") as MonoBehaviour).enabled = true;
		//GetComponent("realocateToChild") as MonoBehaviour).enabled = true;
		(GetComponent("BarraVida") as MonoBehaviour).enabled = true;
		(GetComponent("ElementalPoints") as MonoBehaviour).enabled = true;
		//(GetComponent("InitPlayer") as MonoBehaviour).enabled = true;
		StaticVariables.disconnected = true;
	}

}
