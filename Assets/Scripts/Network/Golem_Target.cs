﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Golem_Target : NetworkBehaviour {

	private NavMeshAgent agent;
	private Transform myTransform;
	public Transform targetTransform;
	private LayerMask raycastLayer;
	private float radius = 25;
	
	// Use this for initialization
	void Start () 
	{
		agent = GetComponent<NavMeshAgent>();
		myTransform = transform;
		raycastLayer = 1<<LayerMask.NameToLayer("Player");
		
		if(isServer)
		{
			StartCoroutine(DoCheck());
		}
	}

	void SearchForTarget()
	{
		if(!isServer)
		{
			return;
		}
		
		if(targetTransform == null)
		{
			Collider[] hitColliders = Physics.OverlapSphere(myTransform.position, radius, raycastLayer);
			
			if(hitColliders.Length>0)
			{
				int randomint = Random.Range(0, hitColliders.Length);
				targetTransform = hitColliders[randomint].transform;
			}
		}
		
		if(targetTransform != null && targetTransform.GetComponent<CapsuleCollider>().enabled == false)
		{
			targetTransform = null;
		}
	}
	
	void MoveToTarget()
	{
		if(targetTransform != null && isServer)
		{
			SetNavDestination(targetTransform);
		}
	}
	
	void SetNavDestination(Transform dest)
	{
		agent.SetDestination(dest.position);
	}
	
	IEnumerator DoCheck()
	{
		for(;;)
		{
			SearchForTarget();
			MoveToTarget();
			yield return new WaitForSeconds(0.2f);
		}
	}

}
