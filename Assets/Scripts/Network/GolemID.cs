﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class GolemID : NetworkBehaviour {

	[SyncVar]public string golemID;
	private Transform myTransform;

	// Use this for initialization
	void Start () 
	{
		myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () 
	{
		SetIdentity();
	}

	void SetIdentity()
	{
		if(myTransform.name == "" || myTransform.name == "OnlineWaterGolem(Clone)" || myTransform.name == "OnlineBoss(Clone)")
		{
			myTransform.name = golemID;
		}
	}

}
