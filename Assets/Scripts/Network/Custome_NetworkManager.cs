﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Custome_NetworkManager : NetworkManager {

	public void StartupHost()
	{
		if(VerifySelection())
		{
			SetPort();
			NetworkManager.singleton.StartHost();
		}
	}

	public void JoinGame()
	{
		if(VerifySelection())
		{
			SetIPAddress();
			SetPort();
			NetworkManager.singleton.StartClient();
		}
	}

	bool VerifySelection()
	{
		Text validate = GameObject.Find("MultiValidate").GetComponent<Text>();
		string charName = GameObject.Find("MultiInputField").transform.FindChild("Text").GetComponent<Text>().text;
		if(charName.Equals ("") && StaticVariables.playerGenre.Equals ("") && StaticVariables.selectedWeapon == StaticVariables.Weapon.None)
		{
			validate.text = "Select your character gender, name and weapon";
		}
		else if (charName.Equals ("")) 
		{
			validate.text = "Type your character name";
		} 
		else if (StaticVariables.playerGenre.Equals ("")) 
		{
			validate.text = "Select your character genre";
		} 
		else if (StaticVariables.selectedWeapon == StaticVariables.Weapon.None)
		{
			validate.text = "Select your character weapon";
		}
		else
		{
			StaticVariables.playerName = charName;
			return true;
		}
		return false;
	}

	void SetIPAddress()
	{
		string ipAdderss = GameObject.Find("InputFieldIPAddress").transform.FindChild("Text").GetComponent<Text>().text;
		NetworkManager.singleton.networkAddress = ipAdderss;
	}

	void SetPort()
	{
		NetworkManager.singleton.networkPort = 7777;
	}

	void OnLevelWasLoaded(int level)
	{
		if(level == 1) StartCoroutine("SetupMenuSceneButtons");
		else if(level == 7) SetupOtherSceneButtons();
	}

	IEnumerator SetupMenuSceneButtons()
	{
		yield return new WaitForSeconds(05f);
		GameObject.Find("StartHost").GetComponent<Button>().onClick.RemoveAllListeners();
		GameObject.Find("StartHost").GetComponent<Button>().onClick.AddListener(StartupHost);

		GameObject.Find("JoinGame").GetComponent<Button>().onClick.RemoveAllListeners();
		GameObject.Find("JoinGame").GetComponent<Button>().onClick.AddListener(JoinGame);
	}

	void SetupOtherSceneButtons()
	{
		GameObject.Find("Disconnect").GetComponent<Button>().onClick.RemoveAllListeners();
		GameObject.Find("Disconnect").GetComponent<Button>().onClick.AddListener(NetworkManager.singleton.StopHost);
	}

}
