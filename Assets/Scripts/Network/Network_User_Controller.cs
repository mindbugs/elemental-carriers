﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
	[RequireComponent(typeof (ThirdPersonCharacter))]
	public class Network_User_Controller : NetworkBehaviour {

		public float rotationSpeed = 4.5f;
		private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
		private Transform m_Cam;                  // A reference to the main camera in the scenes transform
		private Vector3 m_CamForward;             // The current forward direction of the camera
		private Vector3 m_Move;
		private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
		
		private Vector3 m_TargetAngles; 		// for managing mouse rotation speed
		private Vector3 m_FollowAngles;
		private Vector3 m_FollowVelocity;
		private Quaternion m_OriginalRotation;

		private bool m_Crouch;
		
		private void Start()
		{
			// get the transform of the main camera
			if (Camera.main != null)
			{
				m_Cam = Camera.main.transform;
			}
			else
			{
				Debug.LogWarning(
					"Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.");
				// we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
			}
			
			// get the third person character ( this should never be null due to require component )
			m_Character = GetComponent<ThirdPersonCharacter>();
		}
		
		
		private void Update()
		{
			if (isLocalPlayer)
			{
				if (!m_Jump)
				{
					m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
				}
			}

			// This is executed on the sever, and results in a RPC on the client
			CmdMove(m_Move, m_Crouch, m_Jump);
		}
		
		
		// Fixed update is called in sync with physics
		private void FixedUpdate()
		{
			if (isLocalPlayer)
			{
				// read inputs
				float v = CrossPlatformInputManager.GetAxis("Vertical");
				float h = CrossPlatformInputManager.GetAxis("Horizontal");
				
				//para aplicar rotacion con el mouse entonces tenemos que encontrarnos 'idle' tanto en el
				//eje x como en el eje y.
				if(h == 0 && v == 0 ){
					h = CrossPlatformInputManager.GetAxis("Mouse X");
					// wrap values to avoid springing quickly the wrong way from positive to negative
					if (m_TargetAngles.y > 180)
					{
						m_TargetAngles.y -= 360;
						m_FollowAngles.y -= 360;
					}
					if (m_TargetAngles.x > 180)
					{
						m_TargetAngles.x -= 360;
						m_FollowAngles.x -= 360;
					}
					if (m_TargetAngles.y < -180)
					{
						m_TargetAngles.y += 360;
						m_FollowAngles.y += 360;
					}
					if (m_TargetAngles.x < -180)
					{
						m_TargetAngles.x += 360;
						m_FollowAngles.x += 360;
					}
					m_TargetAngles.y += h*rotationSpeed;
					
					
					// smoothly interpolate current values to target angles
					m_FollowAngles = Vector3.SmoothDamp(m_FollowAngles, m_TargetAngles, ref m_FollowVelocity, 0.2f);
					
					
					transform.localRotation = new Quaternion(); 
				}
				
				bool crouch = Input.GetKey(KeyCode.C);
				
				// calculate move direction to pass to character
				if (m_Cam != null)
				{
					// calculate camera relative direction to move:
					m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
					m_Move = v*m_CamForward + h*m_Cam.right;
				}
				else
				{
					// we use world-relative directions in the case of no main camera
					m_Move = v*Vector3.forward + h*Vector3.right;
				}
				#if !MOBILE_INPUT
				// walk speed multiplier
				if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
				#endif
				
				// pass all parameters to the character control script
				m_Character.Move(m_Move, crouch, m_Jump);
				m_Jump = false;
			}
		}

		[Command]
		void CmdMove(Vector3 move, bool crouch, bool jump)
		{
			RpcMove(move, crouch, jump);
		}
		
		
		[ClientRpc]
		void RpcMove(Vector3 move, bool crouch, bool jump)
		{
			if (isLocalPlayer)
				return;
			
			m_Move = move;
			m_Crouch = crouch;
			m_Jump = jump;
			m_Character.Move(move, crouch, jump);
		}

	}
}
